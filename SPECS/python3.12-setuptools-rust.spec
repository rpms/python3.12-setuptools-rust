%global __python3 /usr/bin/python3.12
%global python3_pkgversion 3.12

# RHEL does not have packaged rust libraries
%bcond_with packaged_rust_libraries
# The integration tests depend on the presence of these libraries
%bcond_with integration_tests
# Regex of integration tests to skip.
#  * html-py-ever requires unpackaged rust crates
%global integration_tests_exc '^(html-py-ever)'

Name:           python%{python3_pkgversion}-setuptools-rust
Version:        1.7.0
Release:        2%{?dist}
Summary:        Setuptools Rust extension plugin

License:        MIT
URL:            https://github.com/PyO3/setuptools-rust
Source0:        %{pypi_source setuptools-rust}
Source1:        setup.py
BuildArch:      noarch

BuildRequires:  python3-devel
BuildRequires:  %{py3_dist pytest}
BuildRequires:  python%{python3_pkgversion}-devel
BuildRequires:  python%{python3_pkgversion}-rpm-macros
BuildRequires:  python%{python3_pkgversion}-setuptools > 46.1
BuildRequires:  python%{python3_pkgversion}-semantic_version >= 2.8.2
BuildRequires:  python%{python3_pkgversion}-wheel

Requires:       python%{python3_pkgversion}-semantic_version >= 2.8.2
Requires:       python%{python3_pkgversion}-setuptools >= 62.4

%if 0%{?fedora}
BuildRequires:  rust-packaging
Requires:       rust-packaging
%else
# RHEL has rust-toolset instead of rust-packaging
BuildRequires:  rust-toolset >= 1.45
Requires:       rust-toolset >= 1.45
%endif
%if %{with integration_tests}
BuildRequires:  %{py3_dist cffi}
%endif

%description
Setuptools helpers for Rust Python extensions. Compile and distribute Python
extensions written in Rust as easily as if they were written in C.}


%prep
%autosetup -n setuptools-rust-%{version}
# minimal setup.py to support %%py3_build and %%py3_install
cp %{SOURCE1} .

# remove dependency on typing-extensions and use stdlib instead
# python3.12-typing-extensions is not packaged in RHEL
sed -i '/typing_extensions/d' pyproject.toml
sed -i -e 's/typing_extensions/typing/' \
     setuptools_rust/setuptools_ext.py \
     setuptools_rust/build.py \
     setuptools_rust/extension.py

%cargo_prep

%if %{with integration_tests}
for example in $(ls examples/ | grep -vE %{integration_tests_exc}); do
    cd "examples/${example}"
    %cargo_prep
    cd -
done
%endif


%if %{with integration_tests}
%generate_buildrequires
for example in $(ls examples/ | grep -vE %{integration_tests_exc}); do
    cd "examples/${example}"
    %cargo_generate_buildrequires
    cd - >&2
done
%endif


%build
%py3_build


%install
%py3_install


%check
# Disable tests that require internet access and/or test Windows functionality
%global test_ignores %{shrink:
        not test_adjusted_local_rust_target_windows_msvc
    and not test_get_lib_name_namespace_package
}

%if %{without packaged_rust_libraries}
%global test_ignores %{shrink:%{test_ignores}
    and not test_metadata_contents
    and not test_metadata_cargo_log
}
%endif

%pytest tests/ setuptools_rust/ --import-mode importlib -k '%{test_ignores}'

%if %{with integration_tests}
export %{py3_test_envvars}
%global _pyproject_wheeldir dist
for example in $(ls examples/ | grep -vE %{integration_tests_exc}); do
    cd "examples/${example}"
    %pyproject_wheel
    if [ -d "tests/" ]; then
        %{python3} -m venv venv --system-site-packages
        ./venv/bin/pip install dist/*.whl
        ./venv/bin/python -Pm pytest tests/
    fi
    cd -
done
%endif


%files -n python%{python3_pkgversion}-setuptools-rust
%doc README.md CHANGELOG.md
%license LICENSE
%{python3_sitelib}/setuptools_rust/
%{python3_sitelib}/setuptools_rust-%{version}-py%{python3_version}.egg-info/


%changelog
* Tue Jan 23 2024 Miro Hrončok <mhroncok@redhat.com> - 1.7.0-2
- Rebuilt for timestamp .pyc invalidation mode

* Fri Oct 20 2023 Tomáš Hrnčiar <thrnciar@redhat.com> - 1.7.0-1
- Initial package
- Fedora contributions by:
      Christian Heimes <cheimes@redhat.com>
      Fabio Valentini <decathorpe@gmail.com>
      Gwyn Ciesla <limb@fedoraproject.org>
      Maxwell G <maxwell@gtmx.me>
      Miro Hrončok <miro@hroncok.cz>
      Tomáš Hrnčiar <thrnciar@redhat.com>
